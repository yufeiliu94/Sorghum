#include <c++/7.2.0/memory>
#include "sorghum/cv.hpp"

/**
 * Sorghum::GL
 * 在 OpenGL 基础上实现的 2D 绘图库
 */

namespace Sorghum::CV {

Window::Window(std::string name) : name{name} {
  cv::namedWindow(name);
}

Window::~Window() {
  cv::destroyWindow(name);
}

Window &Window::wait_key() {
  cv::waitKey(0);
  return *this;
}

Window &Window::display(Image &image) {
  cv::imshow(name, *image.data);
}

Image::Image(size_t width, size_t height) :
    width{width}, height{height},
    data{std::make_unique<cv::Mat>(height, width, CV_32FC3)} {}

Image::Image(std::string filename) :
  data{std::make_unique<cv::Mat>(cv::imread(filename))},
  width{static_cast<size_t>(data->cols)},
  height{static_cast<size_t>(data->rows)} {}

cv::Vec3f& Image::at(size_t x, size_t y) {
  return data->at<cv::Vec3f>(y, x);
}

void Image::save_to(std::string filename) {
  cv::imwrite(filename, *data);
}

void Image::read_from(std::string filename) {
  *data = cv::imread(filename);
}

}