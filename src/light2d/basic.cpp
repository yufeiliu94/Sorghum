#include <iostream>
#include <memory>
#include <cmath>
#include "sorghum/cv.hpp"

#define N 64
#define MAX_STEP 10
#define MAX_DISTANCE 2.f
#define EPSILON 1e-6
#define TWO_PI 6.2831853f

float circleSDF(float x, float y, float cx, float cy, float r) {
  float ux = x - cx, uy = y - cy;
  return std::sqrt(ux * ux + uy * uy) - r;
}

float trace(float ox, float oy, float dx, float dy) {
  float t = 0.0f;
  for (int i = 0; i < MAX_STEP && t < MAX_DISTANCE; i++) {
    float sd = circleSDF(ox + dx * t, oy + dy * t, 0.5f, 0.5f, 0.1f);
    if (sd < EPSILON)
      return 2.0f;
    t += sd;
  }
  return 0.0f;
}

float sample(float x, float y) {
  float sum = 0.0f;
  for (int i = 0; i < N; i++) {
    float a = TWO_PI * (i + (float) rand() / RAND_MAX) / N;
    sum += trace(x, y, cosf(a), sinf(a));
  }
  return sum / N;
}

void render(Sorghum::CV::Image &image) {
  std::clog << "Rendering [ " << image.width << " x " << image.height << " ]" << std::endl;
  for (uint64_t y = 0; y < image.height; ++y) {
    for (uint64_t x = 0; x < image.width; ++x) {
      auto val = sample(x / float(image.width), y / float(image.height)) * 255;
      image.at(x, y) = {val, val, val};
    }
  }
  std::clog << "Done" << std::endl;
}

int main(int argc, char *argv[]) {
  auto image = Sorghum::CV::Image(512, 512);
  auto window = Sorghum::CV::Window("Light2D");
  render(image);
  window.display(image).wait_key();
  image.save_to("basic2.png");
  return 0;
}