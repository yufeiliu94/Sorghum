#pragma once

/**
 * Sorghum::CV
 * 在 OpenCV 基础上实现的 2D 绘图库
 */

#include <opencv2/highgui.hpp>
#include "sorghum/vector.hpp"

namespace Sorghum::CV {

struct Image {
  std::unique_ptr<cv::Mat> data;
  const size_t width, height;

  Image() = delete;
  Image(size_t width, size_t height);
  Image(std::string filename);

  cv::Vec3f &at(size_t x, size_t y);
  void save_to(std::string filename);
  void read_from(std::string filename);
};

struct Window {
  const std::string name;

  Window() = delete;
  explicit Window(std::string name);
  ~Window();

  Window &display(Image &image);
  Window &wait_key();
};

}

