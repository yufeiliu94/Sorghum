#pragma once

#include <initializer_list>
#include <type_traits>
#include <algorithm>
#include <cstdint>
#include <cstddef>

/**
 * Sorghum::Vector
 * 用于 Sorghum 的统一向量格式
 */

namespace Sorghum::Vector {

namespace Internal {

template<typename T, uint64_t N, typename ...Ts>
struct _VecTypeTranslator {
  using type = typename _VecTypeTranslator<T, N - 1, T, Ts...>::type;
};

template<typename T, typename ...Ts>
struct _VecTypeTranslator<T, 0ULL, Ts...> {
  using type = std::tuple<Ts...>;
};

}

template<uint64_t N, typename T>
struct vec {
//  std::array<T, N> array;
  T array[N];

  using vec_type = vec<N, T>;
  using tuple_type = typename Internal::_VecTypeTranslator<T, N>::type;

  inline vec(std::initializer_list<T> il) {
    std::copy_n(il.begin(), N, array);
  }

  inline vec(const vec_type &other) {
    std::copy_n(other.array, N, array);
  }

  inline vec() {
    std::fill_n(array, N, T(0));
  }

  inline vec(T value) {
    std::fill_n(array, N, value);
  }

  operator tuple_type() const {
    return tuple_type(array);
  }

#define VECTOR_OPERATOR(name, op)       \
  vec_type &name(const vec_type &rhs) { \
    for (auto i = 0; i < N; ++i) {      \
      array[i] op rhs[i];               \
    }                                   \
    return *this;                       \
  }                                     \
  vec_type &name(const T val) {         \
    for (auto i = 0; i < N; ++i) {      \
      array[i] op val;                  \
    }                                   \
    return *this;                       \
  }

  VECTOR_OPERATOR(operator+=, +=)
  VECTOR_OPERATOR(operator-=, -=)
  VECTOR_OPERATOR(operator*=, *=)
  VECTOR_OPERATOR(operator/=, /=)
#undef VECTOR_OPERATOR

#define VECTOR_OPERATOR(name, op)       \
  vec_type name(const vec_type &rhs) {  \
    vec_type v;                         \
    for (auto i = 0; i < N; ++i) {      \
      v.array[i] = array[i] op rhs[i];  \
    }                                   \
    return v;                           \
  }                                     \
  vec_type name(const T val) {          \
    vec_type v;                         \
    for (auto i = 0; i < N; ++i) {      \
      v.array[i] = array[i] op val;     \
    }                                   \
    return v;                           \
  }

  VECTOR_OPERATOR(operator+, +)
  VECTOR_OPERATOR(operator-, -)
  VECTOR_OPERATOR(operator*, *)
  VECTOR_OPERATOR(operator/, /)
#undef VECTOR_OPERATOR

  bool operator==(const vec_type &other) const {
    bool equals = true;
    for (int i = 0; i < N; ++i) {
      equals &= array[i] == other.array[i];
    }
    return equals;
  }

  bool operator!=(const vec_type &other) const {
    bool not_equals = false;
    for (int i = 0; i < N; ++i) {
      not_equals |= array[i] != other.array[i];
    }
    return not_equals;
  }

};

using vec2i = vec<2, int>;
using vec3i = vec<3, int>;
using vec4i = vec<4, int>;

using vec2f = vec<2, float>;
using vec3f = vec<3, float>;
using vec4f = vec<4, float>;

using vec2d = vec<2, double>;
using vec3d = vec<3, double>;
using vec4d = vec<4, double>;


} // namespace Sorghum::Vector

/**
 * 使 Sorghum::Vector::vec<Size, ElementType> 支持 std::get<Index>, std::tuple_size, std::apply
 */
namespace std {
  template <size_t Index, typename T, uint64_t N>
  T &get(Sorghum::Vector::vec<N, T> &v) {
    return v.array[Index];
  };

  template<typename T, uint64_t N>
  struct tuple_size<Sorghum::Vector::vec<N, T>>: integral_constant<size_t, N> {
    size_t operator()() const {
      return N;
    }
    operator size_t() const {
      return N;
    }
  };
} // namespace std
